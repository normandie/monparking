<?php

namespace App\Tests\DTO\Sublease;

use App\DTO\Sublease\DTOParkingSubleaseTakerList;
use App\Entity\Day;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Tests\Common\Entity\FakeUser;
use Faker\Factory as FakerFactory;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

/**
 * DTOParkingSubleaseTakerListTest
 */
class DTOParkingSubleaseTakerListTest extends TestCase
{
    /**
     * @var FakerFactory
     */
    private $faker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = FakerFactory::create();
    }

    private function getDateTimeAsString(
        string $startDate = '-30 years',
        string $endDate = 'now',
        $timezone = null,
        string $format = 'Y-m-d'
    ): string {
        return ($this->faker->dateTimeBetween($startDate, $endDate, $timezone))->format($format);
    }

    private function getUser(string $role): User
    {
        $user = FakeUser::generate(
            null,
            null,
            null,
            null,
            $role,
            [
                'id' => $this->faker->numberBetween(),
                'parkingNumber' => $this->faker->numberBetween(0, 50),
            ]
        );

        return $user;
    }

    private function getSubleases(User $user, int $count = 50): array
    {
        $subleases = [];

        for ($index = 0; $index < $count; $index++) {
            $subleases[] = new ParkingSublease(
                new Day(new \DateTimeImmutable($this->getDateTimeAsString('-2 years', '-1 week'))),
                $user
            );
        }

        return $subleases;
    }

    private function getDTOParkingSubleaseTakerList(
        ?string $month = null,
        ?User $user = null,
        ?string $role = null,
        ?array $subleases = null
    ): DTOParkingSubleaseTakerList {
        $month = $month ?? $this->getDateTimeAsString('-2 years', '-1 week', null, 'Y-m');
        $role = $role ?? 'roleTenant';
        $user = $user ?? $this->getUser($role);
        $subleases = $subleases ?? $this->getSubleases($user);

        return new DTOParkingSubleaseTakerList(
            $month,
            $user,
            $role,
            $subleases
        );
    }

    public function testConstructor(): void
    {
        $month = $this->getDateTimeAsString('-2 years', '-1 week', null, 'Y-m');
        $role = 'roleTenant';
        $user = $this->getUser($role);
        $subleases = $this->getSubleases($user);

        $takerList = $this->getDTOParkingSubleaseTakerList(
            $month,
            $user,
            $role,
            $subleases
        );

        $this->assertEquals($month, $takerList->getMonth());
        $this->assertEquals($user, $takerList->getContractor());
        $this->assertEquals($role, $takerList->getUserRole());
    }

    public function testUnPaidSumCalculate(): void
    {
        $takerList = $this->getDTOParkingSubleaseTakerList();

        // Un-comment this when constructor doesn't call `unPaidSumCalculate`
        // $this->assertNull($takerList->getUnpaidSum());
        // $takerList->unPaidSumCalculate();
        $this->assertNotNull($takerList->getUnpaidSum());

        $expectedSum = 0;
        $subleasesPerContractor = $takerList->getMonthSubleasesPerContractor();
        if (!empty($subleasesPerContractor)) {
            foreach ($subleasesPerContractor as $sublease) {
                /* @var ParkingSublease $sublease */
                if ($sublease->getIsNotPaid()) {
                    $expectedSum += $sublease->getPrice();
                }
            }
        }

        $this->assertEquals($expectedSum, $takerList->getUnpaidSum());
    }
}
