#!/usr/bin/env bash
# Initialisation
[[ $1 ]] && pushed_branch="$1" || pushed_branch="master"

# @todo: Lancer des tests pour vérifier qu'on peut bien pusher...

# Push de la branche sur Gandi
# push=$(git push origin $pushed_branch)

# Lancement du déploiement du site
# deploy=$(ssh 837005@git.sd6.gpaas.net 'deploy monparking.ch.git')

echo -en "[\033[0;32mok\033[0m] Déploiement du site MonParking terminé"
echo ""

# @todo: Envoyer un message sur Slack pour prévenir les collègues qu'une MAJ a été faite
