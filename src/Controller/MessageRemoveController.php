<?php

namespace App\Controller;

use App\Entity\Messages;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageRemoveController extends AbstractController
{
    /**
     * @Route("/message/remove/{messageId}", name="message_remove", requirements={"\d+"})
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_PREVIOUS_ADMIN')")
     * @Entity("message", expr="repository.find(messageId)")
     * @param EntityManagerInterface $entityManager
     * @param Messages $message
     * @param Request $request
     * @return Response
     */
    public function __invoke(EntityManagerInterface $entityManager, Messages $message, Request $request): Response
    {

        $referer = $request->headers->get('referer', '/');

        $entityManager->remove($message);
        $entityManager->flush();

        $isRefererMessageOpen = strpos($referer, 'open') == true;
//        dd($isRefererMessageOpen);

        if ($isRefererMessageOpen){
            return $this->redirectToRoute('message_receive', ['inboxType' => 'inbox']);
        }else {
            return $this->redirect($referer);
        }
    }
}
