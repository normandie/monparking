<?php

namespace App\Controller;

use App\ControllerHelpers\CalendarData;
use App\ControllerHelpers\Security\SecurityUser;
use App\Repository\MessagesRepository;
use App\Repository\ParkingSubleaseRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Prophecy\Util\ExportUtil;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NavBarCounterController extends AbstractController
{
    /**
     * @Route("/nav/bar/counter", name="nav_bar_counter")
     * @param MessagesRepository $messagesRepository
     * @param UserRepository $userRepository
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception
     */
    public function layout(MessagesRepository $messagesRepository, UserRepository $userRepository,
                           ParkingSubleaseRepository $parkingSubleaseRepository,
                           EntityManagerInterface $entityManager): Response
    {
        $numberOfUnreadMessages = 0;
        $receivableTotalAmount = 0;
        $receivedTotalAmount = 0;
        $receivableAndreceivedTotalAmount = 0;
        $dueTotalAmount = 0;
//        $currentOrNextMonthDueTotalAmount = 0;
        $dueAmountOnNextDueDate = 0;
        $nextDueDate = null;
        $previousMonthDueMoney = null;

//        $totalAmountDueOrPaid = 0;
//        $totalAmountPaid = 0;

        if($this->getUser() !== null) {

            /* @var SecurityUser $securityUser */
            $securityUser = $this->getUser();
            $loggedInUser = $securityUser->getUser();

            if ($this->isGranted('ROLE_ADMINISTRATOR')){
                $grReceiverUser = $userRepository->findOneBy(['username' => 'gr']);
                if ($grReceiverUser !== null) {
                    $unReadMessagesList = $messagesRepository->findReceivedAndNotAcknowledgedByAdministratorAndByUser($loggedInUser, $grReceiverUser);
                }else{
                    $unReadMessagesList = $messagesRepository->findReceivedAndNotAcknowledgedMessages($loggedInUser);
                }
            }else {
                $unReadMessagesList = $messagesRepository->findReceivedAndNotAcknowledgedMessages($loggedInUser);
            }
            $numberOfUnreadMessages = count($unReadMessagesList);


            if ($loggedInUser->getRole() === 'ROLE_RENTER') {

                $perLoggedInReceiverToBePaidSubleases =
                    $parkingSubleaseRepository->findTabMoneyToBeReceivedSubleasesPerUser($loggedInUser);
                foreach ($perLoggedInReceiverToBePaidSubleases as $sublease){
                    $receivableTotalAmount += $sublease->getPrice();
                }

                $perLoggedInReceiverPaidSubleases =
                    $parkingSubleaseRepository->findTabMoneyReceivedSubleasesPerUser($loggedInUser);
                foreach ($perLoggedInReceiverPaidSubleases as $sublease){
                    $receivedTotalAmount += $sublease->getPrice();
                }

                $perLoggedInReceiverToBePaidAndPaidSubleases =
                    $parkingSubleaseRepository->findTabMoneyToBeReceivedAndReceivedSubleasesPerUser($loggedInUser);
                foreach ($perLoggedInReceiverToBePaidAndPaidSubleases as $sublease){
                    $receivableAndreceivedTotalAmount += $sublease->getPrice();
                }
            }

            if ($loggedInUser->getRole() === 'ROLE_TENANT') {
                $currentDate = new DateTimeImmutable('midnight', new DateTimeZone('Europe/Paris'));
                $currentDate = $currentDate->modify('first day of this month');
                $nextDueDate = $currentDate->modify('+ 10 days');
//                $currentMonthString = $currentDate->format('m-Y');
//
//                $previousMonthDate = $currentDate->modify('-1 month');
//                $previousMonthString = $previousMonthDate->format('m-Y');
//
//                $nextMonthDate = $currentDate->modify('+1 month');
//                $nextMonthString = $nextMonthDate->format('m-Y');

                $grRenterUser = $userRepository->findOneBy(['username' => 'gr']);
                $perLoggedInPayerDueSubleases =
                    $parkingSubleaseRepository->findTabMoneyDueSubleasesPerTaker($loggedInUser, $grRenterUser);
                foreach ($perLoggedInPayerDueSubleases as $sublease){
                    $dueTotalAmount += $sublease->getPrice();
                }
//
//                foreach ($perLoggedInPayerDueSubleases as $sublease) {
//                    if (
//                        ($sublease->getUser()->getUsername() !== 'gr')
//                        && (
//                            ($sublease->getDay()->getDate()->format('m-Y') === $currentMonthString)
//                            || ($sublease->getDay()->getDate()->format('m-Y') === $nextMonthString)
//                        )
//                    ){
//                        $currentOrNextMonthDueTotalAmount += $sublease->getPrice();
//                    }
//                }
//
//                $dueAmountOnNextDueDate = $dueTotalAmount - $currentOrNextMonthDueTotalAmount;
//
//
                $calendar = new CalendarData([]);
                $previousMonthDueMoney = $calendar->toStringLowCasePreviousMonth();
//
//                if (!$this->isGranted('ROLE_PREVIOUS_ADMIN')) {
//                    if (($dueAmountOnNextDueDate > 0) && ($loggedInUser->getInsolvencyImmunity() !== true) &&
//                        ($loggedInUser->getIsUserBlockedForInsolvency() !== true)) {
//                        $now = new DateTimeImmutable('midnight', new DateTimeZone('Europe/Paris'));
//                        if ($now > $nextDueDate) {
//                            $loggedInUser->setIsUserBlockedForInsolvency(true);
//                            $entityManager->flush();
//                        }
//                    }
//                }

                $now = new DateTimeImmutable('midnight', new DateTimeZone('Europe/Paris'));

                $toBePaidList = $parkingSubleaseRepository->findTabMoneyDueSubleasesPerTaker($loggedInUser, $grRenterUser);

                foreach ($toBePaidList as $sublease) {
                    if ($sublease->getDueDate() === null) {
                        $sublease->setDueDate();
                    }
                    $monthFirstDayOfDueDate = $now->modify('first day of this month');
                    if ($sublease->getDay()->getDate() < $monthFirstDayOfDueDate)
                        $dueAmountOnNextDueDate += $sublease->getPrice();
                }
                $entityManager->flush();


                if (!empty($toBePaidList)) {
                    $oldestSublease = reset($toBePaidList);
                    if ($oldestSublease !== null) {
                        $firstDueDate = $oldestSublease->getDueDate();
                    }else{
                        $firstDueDate = $now->modify('+ 1 years');
                    }
                }else{
                    $firstDueDate = $now->modify('+1 years');
                }

                if ($now > $firstDueDate) {
                    if ($loggedInUser->getInsolvencyImmunity() !== true) {
                        $loggedInUser->setIsUserBlockedForInsolvency(true);
                        $entityManager->flush();
                    }
                }else{
                    if ($loggedInUser->getIsUserBlockedForInsolvency() === true){
                        $loggedInUser->setIsUserBlockedForInsolvency(false);
                        $entityManager->flush();
                    }
                }
            }

        }

        return $this->render('Layout.twig', [
            'numberOfUnreadMessages' => $numberOfUnreadMessages,
            'receivableTotalAmount' => $receivableTotalAmount,
            'receivedTotalAmount' => $receivedTotalAmount,
            'receivableAndreceivedTotalAmount' => $receivableAndreceivedTotalAmount,
            'dueAmountOnNextDueDate' => $dueAmountOnNextDueDate,
            'nextDueDate' => $nextDueDate,
            'dueTotalAmount' => $dueTotalAmount,
            'previousMonthDueMoney' => $previousMonthDueMoney,
        ]);
    }
}
