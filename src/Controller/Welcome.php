<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Welcome extends AbstractController
{

    /**
     * @return Response
     * @Route("/welcome", name="welcome")
     */
    public function __invoke(): Response
    {
        $test = 2;

        return $this->render('Welcome.twig', [
            'Test' => $test,

        ]);
    }

}
