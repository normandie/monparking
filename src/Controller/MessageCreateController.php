<?php

namespace App\Controller;

use App\ControllerHelpers\Security\SecurityUser;
use App\DTO\DTOMessages;
use App\Entity\Messages;
use App\Entity\User;
use App\Entity\UsersLogsEvents;
use App\Form\MessagesType;
use App\Repository\MessagesRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageCreateController extends AbstractController
{
    /**
     * @Route("/message/create/{receiverId}", name="message_create", requirements={"receiverId" = "\d+"})
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @Entity("receiver", expr="repository.find(receiverId)")
     * @param FormFactoryInterface $formFactory
     * @param Request $request
     * @param User $receiver
     * @param UserRepository $userRepository
     * @param MessagesRepository $messagesRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception
     */
    public function __invoke(FormFactoryInterface $formFactory, Request $request, User $receiver,
                             UserRepository $userRepository, MessagesRepository $messagesRepository,
                             EntityManagerInterface $entityManager): Response
    {

        /* @var SecurityUser $securityUser*/
        $securityUser = $this->getUser();
        $loggedInUser = $securityUser->getUser();

        if ((!$this->isGranted('ROLE_ADMINISTRATOR')) && (!$this->isGranted('ROLE_PREVIOUS_ADMIN'))) {
            $loginEvent = new UsersLogsEvents($loggedInUser,
                new DateTimeImmutable('', new DateTimeZone('Europe/Paris')), 'Création d\'un message');
            $entityManager->persist($loginEvent);
            $entityManager->flush();
        }

        if ($loggedInUser === $receiver){
            throw new LogicException('User can\'t send message to himself');
        }

        $referer = $request->headers->get('referer', '/');

        $form = $formFactory->create(MessagesType::class, new DTOMessages());

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            /* @var Messages $data */
            $data = $form->getData();

            if ($form->isValid()) {

                $text = $data->getMessage();
                $message = new Messages($loggedInUser, $text, $receiver);

                $this->getDoctrine()->getManager()->persist($message);
                $entityManager->flush();
                return $this->redirectToRoute('message_sent');
            }
        }

//            $res = $messagesRepository->findBy(['transmitterId' => $transmitter->getId()]);
//            dd($res);

        return $this->render('message_create/index.html.twig', [
            'Receiver' => $receiver,
            'Referer' => $referer,
            'formular' => $form->createView(),
        ]);
    }
}
