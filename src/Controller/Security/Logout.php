<?php


namespace App\Controller\Security;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class Logout extends AbstractController
{
    /**
     * @Route("/logout", name="logout")
     * Security("is authentified anonymously')")
     * @param Request $request
     */
    public function __invoke(Request $request)
    {

//        $referer = $request->headers->get('referer', '/');
//
//        $session = new Session();
//
//        if(!$session->has('previous')) {
//            $session->set('previous', $referer);
//        }


    }

}
