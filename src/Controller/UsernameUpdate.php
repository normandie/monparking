<?php

namespace App\Controller;


use App\DTO\DTOusernameUpdate;
use App\DTO\DTOUserRenterUpdate;
use App\DTO\DTOUserTenantUpdate;
use App\Entity\User;
use App\Form\RenterUserUpdateType;
use App\Form\TenantUserUpdateType;
use App\Form\UsernameUpdateType;
use App\Repository\UserRepository;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class UsernameUpdate extends AbstractController
{
    /**
     * @Route("/user/usernameUpdate/{userId}", name="username_update")
     * @Entity("user", expr="repository.find(userId)")
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param UserRepository $userRepository
     * @param User $user
     * @return Response
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     */
    public function __invoke(Request $request, FormFactoryInterface $formFactory, UserRepository $userRepository, User $user): Response
    {
        $referer = $request->headers->get('referer', '/');

        $securityUser = $this->getUser();

        /**
         * @var User $loggedInUser
         */
        $loggedInUser = $securityUser->getUser();

        $chosenUser = $userRepository->find($user->getId());

        if ($loggedInUser !== $chosenUser){
            throw new LogicException('Logged in user must be the same as the chosen user');
        }

        if ($chosenUser->getRole() === 'ROLE_RENTER' || $chosenUser->getRole() === 'ROLE_TENANT') {
            $DTOUser = new DTOusernameUpdate();
        }else{
            throw new LogicException('User role must be ROLE_RENTER or ROLE_TENANT');
        }

        $DTOUser->setUsername($chosenUser->getUsername());
        $DTOUser->setPhoneNumber($chosenUser->getPhoneNumber());
        $DTOUser->setPayPalEmail($chosenUser->getPayPalEmail());
        $DTOUser->setRevolutPhoneNumber($chosenUser->getRevolutPhoneNumber());

        $form = $formFactory->create(UsernameUpdateType::class, $DTOUser);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $data = $form->getData();

            $newUsername = $data->getUsername();

            $existingUser = $userRepository->findOneBy(['username' => $newUsername]);

            if ($existingUser !== null && $chosenUser->getUsername() !== $newUsername) {
                $form->addError(new FormError('Nom d\'utilisateur déjà utilisé'));
            }

            if ($form->isValid()) {

                /* @var DTOusernameUpdate $data */
                $newUsername = $data->getUsername();
                $newPhoneNumber = $data->getPhoneNumber();
                $newPayPalEmail = $data->getPayPalEmail();
                $newRevolutPhoneNumber = $data->getRevolutPhoneNumber();
                $chosenUser->changeUsername($newUsername);
                $chosenUser->changePhoneNumber($newPhoneNumber);
                $chosenUser->changePayPalEmail($newPayPalEmail);
                $chosenUser->changeRevolutPhoneNumber($newRevolutPhoneNumber);

                $this->getDoctrine()->getManager()->persist($chosenUser);
                $this->getDoctrine()->getManager()->flush();
                return $this->redirectToRoute('user_consulting', ['userId' => $user->getId()]);
            }
        }

        return $this->render('UsernameUpdate.twig', [
            'formular' => $form->createView(),
            'ExistingUser' => $chosenUser,
            'Referer' => $referer,
        ]);

    }
}
