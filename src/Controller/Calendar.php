<?php


namespace App\Controller;


use App\ControllerHelpers\CalendarData;
use App\ControllerHelpers\DayData;
use App\ControllerHelpers\Security\SecurityUser;
use App\Entity\Day;
use App\Entity\User;
use App\Repository\DayRepository;
use App\Repository\ParkingSubleaseRepository;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class Calendar extends AbstractController
{

    /**
     * @param DayRepository $dayRepository
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param int $year
     * @param int $month
     * @return Response
     * @throws Exception
     * @Route("/calendar/{year}/{month}", name="calendar", requirements={"year" = "\d{4}", "month" = "\d{1,2}"})
     */
    public function __invoke(DayRepository $dayRepository, ParkingSubleaseRepository $parkingSubleaseRepository, int $year, int $month): Response
    {

        if ($month < 1 || $month > 12) { throw new BadRequestHttpException('Month must be between 1 and 12'); }

        $today = new DateTimeImmutable('midnight', new DateTimeZone('Europe/Paris'));
//        $currentDate = new DateTimeImmutable('midnight', new DateTimeZone('Europe/Paris'));

        /**
         * Gandi server displays two hours less than Swiss time
         */
//        $today = $today->modify('-2 hours');

        $todayPlusOneMonth = $today->modify('+1 month');

        /* @var User $user */
        $user = null;

        $isUserDefined = ($this->getUser() !== null);

        $currentYear = new DateTimeImmutable('midnight', new DateTimeZone('Europe/Paris'));
        $currentYearPlusOneMonth = $currentYear->modify('+1 month');
        $currentYearPlusOneYear = $currentYear->modify('+1 year');
        $currentYearString = $currentYear->format('Y');

        $stringMonth = (string)$month;
        $stringYear = (string)$year;

//        $currentDate = new DateTimeImmutable('midnight');
//        $currentYearLessOne = $currentDate->modify('-1 month');
//        $currentDateString = $currentYearLessOne->format('m');
//        dd($currentDateString);


        $currentMonthInt = (int)$currentYear->format('m');
        $nextMonthInt = (int)$currentYearPlusOneMonth->format('m');
        $currentYearInt = (int)$currentYear->format('Y');
        $nextYearInt = (int)$currentYearPlusOneYear->format('Y');

        $previousYear = $currentYear->modify('-1 year');
        $previousYearString = $previousYear->format('Y');

        if ($isUserDefined) {
            $securityUser = $this->getUser();
            $user = $securityUser->getUser();
        }

        $allDays = $dayRepository->findAll();

        $calendar = new CalendarData($allDays, $year, $month);

        /**
         * $dayDatalist contains the same dates as $allDays but $dayDataList is confined to a single current month
         * @var DayData[] $dayDataList
         */
        $dayDataList = $calendar->getDayDataList();

//        $test = $parkingSubleaseRepository->findTabIsSubleaseTakenByDate($year, $month);
//        dd($test);

        $currentMonth = new DateTimeImmutable('midnight', new DateTimeZone('Europe/Paris'));
        $firstDayOfCurrentMonth = $currentMonth->modify('first day of this month');
        $lastDayOfCurrentMonth = $currentMonth->modify('last day of this month');
        $takenDaysOfCurrentMonth[][] = [];

        $parkingSubleasesRelatedToCurrentMonth = $parkingSubleaseRepository->findTabIsSubleaseTakenByDate($stringYear, $stringMonth);

        foreach ($parkingSubleasesRelatedToCurrentMonth as $parkingSublease){
            if ($parkingSublease->getIsTaken()) {
                $stringDate = $parkingSublease->getDay()->getDate()->format('Y-m-d');
                if (!isset($takenDaysOfCurrentMonth[$stringDate])) {
                    $takenDaysOfCurrentMonth[$stringDate] = 0;
                    $takenDaysOfCurrentMonth[$stringDate] += 1;
                }else{
                    $takenDaysOfCurrentMonth[$stringDate] += 1;
                }
            }
        }




        $currentMonthTotalOpenToSublease = $calendar->getTotalOpenToSubleaseDaysQuantityPerDayDateList($dayDataList, $today);
//        dd($currentMonthTotalOpenToSublease);

        $currentMonthTotalOpenToSubleaseForTenant = null;

        if ($isUserDefined) {
            /**
             * @var SecurityUser $securityUser
             */
            $securityUser = $this->getUser();

            /* @var User $user */
            $takerUser = $securityUser->getUser();

            if ($takerUser->getRole() === 'ROLE_TENANT') {
                $currentMonthTotalOpenToSubleaseForTenant = $calendar->getTotalByTakerOpenToSubleaseDaysQuantityPerDayDateList($dayDataList, $today, $parkingSubleaseRepository, $takerUser);
            }
        }

        if (!$isUserDefined || $this->isGranted('ROLE_ADMINISTRATOR') || $this->isGranted('ROLE_TENANT') || $this->isGranted('ROLE_RENTER')) {
            $openToSubleaseList = $calendar->getOpenToSubleaseDaysQuantityPerDayDateList($dayDataList);
        }else{
            $openToSubleaseList = null;
        }

        if ($this->isGranted('ROLE_ADMINISTRATOR')) {
            $takenOrOpenedSubleaseList = $calendar->getTakenOrOpenedSubleaseByDateList($dayDataList);
        }else{
            $takenOrOpenedSubleaseList = null;
        }

        if ($this->isGranted('ROLE_RENTER')){
            $isExistingOrHasExistedSubleaseByDayAndByUserList = $calendar->getExistingOrHasExistedOpenedSubleaseByDayAndByUserList($dayDataList, $parkingSubleaseRepository, $user);
            $isExistingOpenedSubleaseByDayAndByUserList = $calendar->getExistingOpenedSubleaseByDayAndByUserList($dayDataList, $parkingSubleaseRepository, $user);

        }else{
            $isExistingOrHasExistedSubleaseByDayAndByUserList = null;
            $isExistingOpenedSubleaseByDayAndByUserList = null;
        }


        if ($this->isGranted('ROLE_TENANT')){
            $IsSubleaseOpenedByDayList = $calendar->getIsSubleaseOpenedByDay($dayDataList, $parkingSubleaseRepository);
            $isExistingTakenSubleaseByDayAndByTakerList = $calendar->getExistingTakenSubleaseByDayAndByTakerList($dayDataList, $parkingSubleaseRepository, $user);
        }else{
            $IsSubleaseOpenedByDayList = null;
            $isExistingTakenSubleaseByDayAndByTakerList = null;
        }



        if ($this->isGranted('ROLE_RENTER')) {
            return $this->render('CalendarConsulting.twig', [
                'Calendar' => $calendar,
                'ParkingSubleaseRepository' => $parkingSubleaseRepository,
                'AllDays' => $allDays,
                'User' => $user,
                'Today' => $today,
                'TodayPlusOneMonth' => $todayPlusOneMonth,
                'CurrentYearString' => $currentYearString,
                'PreviousYearString' => $previousYearString,
                'IsExistingOrHasExistedSubleaseByDayAndByUserList' => $isExistingOrHasExistedSubleaseByDayAndByUserList,
                'IsExistingOpenedSubleaseByDayAndByUserList' => $isExistingOpenedSubleaseByDayAndByUserList,
                'TakenDaysOfCurrentMonth' => $takenDaysOfCurrentMonth,
                'CurrentMonthTotalOpenToSublease' => $currentMonthTotalOpenToSublease,
                'CurrentMonthInt' => $currentMonthInt,
                'NextMonthInt' => $nextMonthInt,
                'NextYearInt' => $nextYearInt,
                'CurrentYearInt' => $currentYearInt,
                'SelectedYearInt' => $year,
                'SelectedMonthInt' => $month,
                'OpenToSubleaseList' => $openToSubleaseList,
                'TakenOrOpenedSubleaseList' => $takenOrOpenedSubleaseList,
            ]);
        }


        if ($this->isGranted('ROLE_TENANT')) {
            return $this->render('CalendarConsulting.twig', [
                'Calendar' => $calendar,
                'ParkingSubleaseRepository' => $parkingSubleaseRepository,
                'AllDays' => $allDays,
                'User' => $user,
                'Today' => $today,
                'TodayPlusOneMonth' => $todayPlusOneMonth,
                'CurrentYearString' => $currentYearString,
                'PreviousYearString' => $previousYearString,
                'IsSubleaseOpenedByDayList' => $IsSubleaseOpenedByDayList,
                'IsExistingTakenSubleaseByDayAndByTakerList' => $isExistingTakenSubleaseByDayAndByTakerList,
                'CurrentMonthTotalOpenToSublease' => $currentMonthTotalOpenToSublease,
                'CurrentMonthTotalOpenToSubleaseForTenant' => $currentMonthTotalOpenToSubleaseForTenant,
                'TakenDaysOfCurrentMonth' => $takenDaysOfCurrentMonth,
                'CurrentMonthInt' => $currentMonthInt,
                'NextMonthInt' => $nextMonthInt,
                'NextYearInt' => $nextYearInt,
                'CurrentYearInt' => $currentYearInt,
                'SelectedYearInt' => $year,
                'SelectedMonthInt' => $month,
                'OpenToSubleaseList' => $openToSubleaseList,
                'TakenOrOpenedSubleaseList' => $takenOrOpenedSubleaseList,
            ]);
        }


        if ($this->isGranted('ROLE_ADMINISTRATOR')) {
            return $this->render('CalendarConsulting.twig', [
                'Calendar' => $calendar,
                'ParkingSubleaseRepository' => $parkingSubleaseRepository,
                'AllDays' => $allDays,
                'User' => $user,
                'Today' => $today,
                'TodayPlusOneMonth' => $todayPlusOneMonth,
                'CurrentYearString' => $currentYearString,
                'PreviousYearString' => $previousYearString,
                'OpenToSubleaseList' => $openToSubleaseList,
                'TakenDaysOfCurrentMonth' => $takenDaysOfCurrentMonth,
                'TakenOrOpenedSubleaseList' => $takenOrOpenedSubleaseList,
                'CurrentMonthTotalOpenToSublease' => $currentMonthTotalOpenToSublease,
                'CurrentMonthInt' => $currentMonthInt,
                'NextMonthInt' => $nextMonthInt,
                'NextYearInt' => $nextYearInt,
                'CurrentYearInt' => $currentYearInt,
                'SelectedYearInt' => $year,
                'SelectedMonthInt' => $month,
            ]);
        }

        return $this->render('CalendarConsulting.twig', [
            'Calendar' => $calendar,
            'Today' => $today,
            'TodayPlusOneMonth' => $todayPlusOneMonth,
            'OpenToSubleaseList' => $openToSubleaseList,
            'CurrentMonthTotalOpenToSublease' => $currentMonthTotalOpenToSublease,
            'TakenDaysOfCurrentMonth' => $takenDaysOfCurrentMonth,
            'CurrentMonthInt' => $currentMonthInt,
            'NextMonthInt' => $nextMonthInt,
            'NextYearInt' => $nextYearInt,
            'CurrentYearInt' => $currentYearInt,
            'SelectedYearInt' => $year,
            'SelectedMonthInt' => $month,
        ]);
    }


    /**
     * @Route("/", name="calendar_redirect")
     */
    public function redirectToCalendar()
    {
        $year = date('Y');
        $month = date('m');

        return $this->redirectToRoute('calendar',[
            'year' => $year,
            'month' => $month,
        ]);
    }

}

