<?php


namespace App\Controller;


use App\Entity\User;
use App\Repository\DayRepository;
use App\Repository\ParkingSubleaseRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class DayTake extends AbstractController
{

    const WITHPRICE = true;

    /**
     * @param string $dateString
     * @param EntityManagerInterface $entityManager
     * @param DayRepository $dayRepository
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param AuthorizationCheckerInterface $authChecker
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_TENANT')")
     * @Route("/daytake/{dateString}", name="day_take", requirements={"\d{4}\-\d{2}\-\d{2}"})
     */
    public function __invoke(string $dateString, EntityManagerInterface $entityManager, DayRepository $dayRepository, ParkingSubleaseRepository $parkingSubleaseRepository, AuthorizationCheckerInterface $authChecker) : Response
    {

        $securityUser = $this->getUser();

        /* @var User $user */
        $user = $securityUser->getUser();

        $isLoggedInUserEnabled = $user->getIsUserEnabled();

        $isLoggedInUserBlockedForInsolvency = $user->getIsUserBlockedForInsolvency();

        if (!$isLoggedInUserEnabled){
            throw $this->createAccessDeniedException('Logged in user has been disabled');
        }

        if ($isLoggedInUserBlockedForInsolvency){
            throw $this->createAccessDeniedException('Logged in user has been blocked for unsolvency');
        }


        $today = (new DateTimeImmutable())->setTime(0, 0);

        /**
         * Gandi server displays two hours less than Swiss time
         */


        /**
         * Creation of a DateTimeImmutable variable from the passed as a parameter string type $dateString variable
         */
        $dateToBeTaken = new DateTimeImmutable($dateString);

        /**
         * Creation of a Day variable from the DateTimeImmutable type $dateToBeTaken variable
         */
        $dayToBeTaken = $dayRepository->findOneBy(['date' => $dateToBeTaken]);


        $parkingSubleaseToBeTaken = null;

        /**
         * Check that the chosen day is concerned by at least one parking sublease proposal
         */
        if ($dayToBeTaken->getIsFreeParkingSubleaseByDayQuantity() > 0) {


            /**
             * check if sublease is not happening in the past
             */
            $isDateToBeTakenPriorToToday = $dateToBeTaken < $today;

            if ($isDateToBeTakenPriorToToday) {
                throw new LogicException('Le jour souhaité pour l\'emprunt est situé dans le passé');
            }


            /**
             * check that the taker user is not trying to sublease more than one parking the same day
             */
            $hasUserAlreadyAParkingTheSameDay = ($parkingSubleaseRepository->findIsSubleaseTakenByDayAndByTaker($dayToBeTaken->getId(), $user->getId()) !== null);

            if ($hasUserAlreadyAParkingTheSameDay) {
                throw new LogicException('Un parking a déjà été emprunté pour ce jour');
            }


            /**
             * Declaration of the variable containing the oldest sublease proposal for the chosen day
             */
            $parkingSubleaseToBeTaken = $parkingSubleaseRepository->findIsSubleaseOpenedByDay($dayToBeTaken->getId());

            /**
             * Check that there is at least one sublease proposal for the chosen day
             */
            if ($parkingSubleaseToBeTaken !== null) {

                /**
                 * Check that the found sublease proposal is in opened modus and not in taken modus
                 */
                $isSubleaseTaken = $parkingSubleaseToBeTaken->getIsTaken();

                if ($isSubleaseTaken) {
                    throw new LogicException('Parking déjà pris');
                }

                $parkingSubleaseToBeTaken->setTaker($user);

                $parkingSubleaseToBeTaken->setIsTaken();

                $parkingSubleaseToBeTaken->setDueDate();

                $dayToBeTaken->isFreeParkingSubleaseByDayDecreaseByOne();

                if (self::WITHPRICE) {
                    $parkingSubleaseToBeTaken->getUser()->sellParking($parkingSubleaseToBeTaken);
                    $parkingSubleaseToBeTaken->getTaker()->buyParking($parkingSubleaseToBeTaken);
                }

                $this->getDoctrine()->getManager()->persist($parkingSubleaseToBeTaken);
                $this->getDoctrine()->getManager()->persist($dayToBeTaken);

                $entityManager->flush();
            }
        }

        return $this->redirectToRoute(
            'calendar', [
            'year' => $dateToBeTaken->format('Y'),
            'month' => $dateToBeTaken->format('m'),
        ]);

    }

}
