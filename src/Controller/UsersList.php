<?php


namespace App\Controller;


use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class UsersList extends AbstractController
{

    /**
     * @param UserRepository $userRepository
     * @param Request $request
     * @return Response
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @Route("/usersList/", name="users_list")
     */
    public function __invoke(UserRepository $userRepository, Request $request): Response
    {
//        $referer = $request->headers->get('referer', '/');

        $userList = $userRepository->getUsersList();
//        dump($userList);die;

        return $this->render('UsersList.twig', [
            'UserList' => $userList,
//            'Referer' => $referer,
        ]);
    }
}
