<?php


namespace App\Controller;


use App\Entity\User;
use App\Repository\ParkingSubleaseRepository;
use DateTimeImmutable;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TenantRentals extends AbstractController
{

    /**
     * @var float
     */
    private $currentYearFirstQuarterTableAmount = 0;

    /**
     * @var float
     */
    private $currentYearSecondQuarterTableAmount = 0;

    /**
     * @var float
     */
    private $currentYearThirdQuarterTableAmount = 0;

    /**
     * @var float
     */
    private $currentYearFourthQuarterTableAmount = 0;


    /**
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param Request $request
     * @param string $sort
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_TENANT')")
    //     * @Route("/myTenantRentals/{sort}", name="my_tenant_rentals", requirements={"sort" = "[a-z]+"})
     * @Route("/myTenantRentals/{sort}", name="my_tenant_rentals")
     */
    public function __invoke(ParkingSubleaseRepository $parkingSubleaseRepository, Request $request, string $sort): Response
    {

        $securityUser = $this->getUser();

        /* @var User $user */
        $user = $securityUser->getUser();

        $referer = $request->headers->get('referer', '/');

        $isRefererCalendar = strpos($referer, 'calendar') !== false;

        /**
         * Period within one month
         */
        $today = new DateTimeImmutable('midnight');

        /**
         * Gandi server displays two hours less than Swiss time
         */

        $todayPlusOneMonth = $today->modify('+1 month');

        if ($sort === 'byRenter') {
            $currentPeriodSubleasesList = $parkingSubleaseRepository->findTabIsSubleaseTakenByTakerBetweenTwoDatesAndOrderedByRenter($user->getId(), $today, $todayPlusOneMonth);
        }elseif ($sort === 'byDates') {
            $currentPeriodSubleasesList = $parkingSubleaseRepository->findTabIsSubleaseTakenByTakerBetweenTwoDates($user->getId(), $today, $todayPlusOneMonth);
        }else{
            $currentPeriodSubleasesList = $parkingSubleaseRepository->findTabIsSubleaseTakenByTakerBetweenTwoDates($user->getId(), $today, $todayPlusOneMonth);
        }


        /**
         * Begin period determination
         */
        $currentYear = new DateTimeImmutable('midnight');
        $currentYearString = $currentYear->format('Y');

        $previousYear = $currentYear->modify('-1 year');
        $previousYearString = $previousYear->format('Y');

        $beginPeriod = $currentYear->modify('first day of January this year');

        $subleasesList = $parkingSubleaseRepository->findTabIsSubleaseTakenByTaker($user->getId(), $beginPeriod);


        return $this->render('TenantRentals.twig', [
            'SubleasesList' => $subleasesList,
            'User' => $user,
            'Referer' => $referer,
            'CurrentYearString' => $currentYearString,
            'PreviousYearString' => $previousYearString,
            'CurrentPeriodSubleasesList' => $currentPeriodSubleasesList,
            'IsRefererCalendar' => $isRefererCalendar,
            'Sort' => $sort,
            'Today' => $today,
        ]);

    }
}
