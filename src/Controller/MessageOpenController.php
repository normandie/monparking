<?php

namespace App\Controller;

use App\Entity\Messages;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageOpenController extends AbstractController
{
    /**
     * @Route("/message/open/{messageId}/{inboxType}", name="message_open", requirements={"messageID" = "\d+", "inboxType" = "[a-z-A-Z]+"})
     * @Entity("message", expr="repository.find(messageId)")
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT') or is_granted('ROLE_ADMINISTRATOR')")
     * @param string $inboxType
     * @param Messages $message
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function __invoke(string $inboxType, Messages $message, EntityManagerInterface $entityManager, Request $request): Response
    {
        $url = $this->generateUrl('message_receive', ['inboxType' => $inboxType]);

        if ($this->getUser()->getUser() !== $message->getReceiver()
            && $this->getUser()->getUser() !== $message->getTransmitter()
            && !$this->isGranted('ROLE_ADMINISTRATOR')){
            throw new LogicException('Logged in user not authorized to open this message !');
        }

        if (($message->getReceiptAcknowledgement() !== true || $message->getReceiptAcknowledgement() === null) &&
            (!$this->isGranted('ROLE_PREVIOUS_ADMIN'))){
            $message->setReceiptAcknowledgement(true);
            $message->setReadingDate(new DateTimeImmutable());
        }

        $this->getDoctrine()->getManager()->persist($message);
        $entityManager->flush();

        return $this->render('message_open/index.html.twig', [
            'message' => $message,
            'url' => $url,
        ]);
    }
}
