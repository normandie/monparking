<?php


namespace App\DTO\Sublease;

use App\Entity\ParkingSublease;
use App\Entity\User;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use Symfony\Component\Validator\Constraints\Date;

class DTOParkingSubleaseTakerList
{
    private $contractor;
    private $userRole;
    private $month;
    private $subleases;
    private $unPaidSubleasesSum;
    private $monthSubleasesPerContractor;
    private $monthSubleasesPerContractorIntermediateTotal;
    private $monthSubleasesPerContractorTotal;

    public function __construct(string $month, User $contractor, string $userRole, array $subleases)
    {
        $this->contractor = $contractor;
        $this->userRole = $userRole;
        $this->month = $month;
        $this->subleases = $subleases;
        $this->subleasesPerMonthAndContractorFulfillment();
        $this->unPaidSumCalculate();
    }

    private function calculateIntermediateSum(): void
    {
        $this->monthSubleasesPerContractorIntermediateTotal = 0;
        $atLeastOneIsPaid = false;
        $atLeastOneIsNotPaid = false;

        if (!empty($this->monthSubleasesPerContractor)) {

            /* @var ParkingSublease $sublease */
            foreach ($this->monthSubleasesPerContractor as $sublease) {
                if ($sublease->getUser()->getUsername() !== 'gr') {
                    if ($sublease->getIsPaid() && $atLeastOneIsPaid === false) {
                        $atLeastOneIsPaid = true;
                    }
                }
            }

            /* @var ParkingSublease $sublease */
            foreach ($this->monthSubleasesPerContractor as $sublease) {
                if ($sublease->getUser()->getUsername() !== 'gr') {
                    if ($sublease->getIsNotPaid() && $atLeastOneIsNotPaid === false) {
                        $atLeastOneIsNotPaid = true;
                    }
                }
            }
        }

        if ($atLeastOneIsPaid === true && $atLeastOneIsNotPaid === true){
            /* @var ParkingSublease $sublease */
            foreach ($this->monthSubleasesPerContractor as $sublease) {
                if ($sublease->getUser()->getUsername() !== 'gr') {
                    if ($sublease->getIsNotPaid()) {
                        $this->monthSubleasesPerContractorIntermediateTotal += $sublease->getPrice();
                    }
                }
            }
        }
    }


    public function unPaidSumCalculate(): void
    {
        $this->unPaidSubleasesSum = 0;

        if (!empty($this->monthSubleasesPerContractor)) {
            foreach ($this->monthSubleasesPerContractor as $sublease) {

                /* @var ParkingSublease $sublease */
                if ($sublease->getIsNotPaid()) {
                    $this->unPaidSubleasesSum += $sublease->getPrice();
                }
            }
        }
    }


    public function getUnpaidSum(): float
    {
        return $this->unPaidSubleasesSum;
    }

    public function getIntermediateSum(): float
    {
        return $this->monthSubleasesPerContractorIntermediateTotal;
    }

    private function subleasesPerMonthAndContractorFulfillment(): void
    {
        $this->monthSubleasesPerContractorTotal = 0;

        if ($this->userRole === 'roleRenter') {
            /* @var ParkingSublease $sublease */
            foreach ($this->subleases as $sublease) {
                if (($sublease->getTaker()->getId() === $this->contractor->getId())
                    && ($sublease->getDay()->getDate()->format('Y-m') === $this->month)) {
                    $this->monthSubleasesPerContractor[] = $sublease;
                    if ($sublease->getUser()->getUsername() !== 'gr') {
                        $this->monthSubleasesPerContractorTotal += $sublease->getPrice();
                    }
                }
            }
            if (!empty($this->monthSubleasesPerContractor)) {
                $this->monthSubleasesPerContractor = array_reverse($this->monthSubleasesPerContractor);
            }
        } elseif ($this->userRole === 'roleTenant') {
            /* @var ParkingSublease $sublease */
            foreach ($this->subleases as $sublease) {
                if (($sublease->getUser()->getId() === $this->contractor->getId())
                    && ($sublease->getDay()->getDate()->format('Y-m') === $this->month)) {
                    $this->monthSubleasesPerContractor[] = $sublease;
                    if($sublease->getUser()->getUsername() !== 'gr') {
                        $this->monthSubleasesPerContractorTotal += $sublease->getPrice();
                    }
                }
            }
            if (!empty($this->monthSubleasesPerContractor)) {
                $this->monthSubleasesPerContractor = array_reverse($this->monthSubleasesPerContractor);
            }
        }
    }



    public function getMonth(): string
    {
        return $this->month;
    }


    public function getDueDate(): DateTimeImmutable
    {
        try {
            $dateItem = new DateTimeImmutable($this->month, new DateTimeZone('Europe/Paris'));
        } catch (Exception $e) {
            return null;
        }
        $dateItem = $dateItem->modify('last day of this month');
        $dateItem = $dateItem->modify('+ 11 days');
        return $dateItem;
    }

    public function getBeginDueDate(): DateTimeImmutable
    {
        try {
            $dateItem = new DateTimeImmutable($this->month, new DateTimeZone('Europe/Paris'));
        } catch (Exception $e) {
            return null;
        }
        $dateItem = $dateItem->modify('last day of this month');
        $dateItem = $dateItem->modify('+ 1 day');
        return $dateItem;
    }

    public function checkIfMonthIsPaid(): bool
    {
        $isMonthPaid = true;

        /* @var ParkingSublease $sublease */
        foreach ($this->monthSubleasesPerContractor as $sublease){
            if ($isMonthPaid === true){
                if ($sublease->getIsNotPaid()){
                    $isMonthPaid = false;
                }
            }
        }
        return $isMonthPaid;
    }

    public function getPaymentDate(): DateTimeImmutable
    {
        $subleases = $this->monthSubleasesPerContractor;

        /* @var ParkingSublease $lastSublease */
        $lastSublease = end($subleases);
        return $lastSublease->getPaidAt();
    }


    public function getMonthSubleasesPerContractorTotal(): int
    {
        return $this->monthSubleasesPerContractorTotal;
    }

    public function getMonthSubleasesPerContractor(): array
    {
        if (empty($this->monthSubleasesPerContractor)){
            return [];
        }

        return $this->monthSubleasesPerContractor;
    }

    public function getContractor(): User
    {
        return $this->contractor;
    }

    public function getUserRole(): string
    {
        return $this->userRole;
    }

}
