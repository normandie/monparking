<?php


namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class DTOMessages
{

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 2000,
     *      minMessage = "Le message doit contenir au moins 2 caractères",
     *      maxMessage = "Le message ne peut pas dépasser 2000 caractères",
     * )
     */
    private $message;


    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }


    /**
     * @param string $text
     */
    public function setMessage(string $text): void
    {
        $this->message = $text;
    }

}
