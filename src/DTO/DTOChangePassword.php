<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


class DTOChangePassword
{

    //=========================================================================
    // Properties
    //=========================================================================


    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 8,
     *      max = 50,
     *      minMessage = "Le mot de passe doit contenir au moins 8 caractères",
     *      maxMessage = "Le mot de passe doit ne peut pas dépasser 50 caractères",
     * )
     */
    private $password;

    /**
     * @return string
     */
    public function getPassword() : ?string
    {
        return $this->password;
    }

    public function setPassword($password) : void
    {
//        $this->password = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);
        $this->password = $password;
    }

}
