<?php

namespace App\Repository;

use App\Entity\Day;
use App\Entity\ParkingSublease;
use App\Entity\User;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Common\Persistence\ManagerRegistry;
use Exception;

/**
 * @method ParkingSublease|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParkingSublease|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParkingSublease[]    findAll()
 * @method ParkingSublease[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @throws Exception
 */
class ParkingSubleaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParkingSublease::class);
    }


    /**
     * @param int $dayId
     * @return ParkingSublease[]
     */
    public function findSubleaseByDay(int $dayId){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('day.id = :dayId')
            ->setParameter('dayId', $dayId);

        return $query->getQuery()->getResult();
    }


    /**
     * @param int $dayId
     * @param int $loggedUserId
     * @return ParkingSublease|null
     */
    public function findOpenedSubleaseByDayAndByUser(int $dayId, int $loggedUserId){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.day', 'day')
            ->innerJoin('parking_sublease.user', 'user')
            ->andWhere('day.id = :dayId')
            ->setParameter('dayId', $dayId)
            ->andWhere('user.id = :loggedUser')
            ->setParameter('loggedUser', $loggedUserId);

        try {
            return $query->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return null;
    }


    /**
     * @param int $dayId
     * @return ParkingSublease
     */
    public function findTabExistingOrHasExistedOpenedSubleaseByDay(int $dayId){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('day.id = :dayId')
            ->setParameter('dayId', $dayId);

        return $query->getQuery()->getResult();
    }


    /**
     * @param int $dayId
     * @param int $loggedUserId
     * @return ParkingSublease|null
     */
    public function findIsSubleaseOpenedByDayAndByUser(int $dayId, int $loggedUserId){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.day', 'day')
            ->innerJoin('parking_sublease.user', 'user')
            ->andWhere('parking_sublease.isSubleaseOpened = :isSubleaseOpened')
            ->andWhere('day.id = :id')
            ->andWhere('user.id = :loggedUser')
            ->setParameter('id', $dayId)
            ->setParameter('loggedUser', $loggedUserId)
            ->setParameter('isSubleaseOpened', true)
            ->orderBy('parking_sublease.creationDate');

        try {
            return $query->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return null;
    }

//Et faire un getOneOrNullResult à la place du bordel


    /**
     * @param int $dayId
     * @param int $loggedTakerUserId
     * @return ParkingSublease|null
     */
    public function findIsSubleaseTakenByDayAndByTaker(int $dayId, int $loggedTakerUserId){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->setParameter('id', $dayId)
            ->setParameter('loggedTakerUser', $loggedTakerUserId)
            ->innerJoin('parking_sublease.day', 'day')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->andWhere('parking_sublease.isSubleaseOpened = :isSubleaseOpened')
            ->andWhere('day.id = :id')
            ->andWhere('taker.id = :loggedTakerUser')
            ->setParameter('isSubleaseOpened', false)
            ->orderBy('parking_sublease.creationDate');

        try {
            return $query->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return null;

//        return $query->getQuery()->getResult();

    }


    /**
     * @param int $dayId
     * @return ParkingSublease[]
     */
    public function findIsSubleaseTakenByDay(int $dayId){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('day.id = :id')
            ->setParameter('id', $dayId)
            ->andWhere('parking_sublease.isSubleaseOpened = :isSubleaseOpened')
            ->setParameter('isSubleaseOpened', false)
            ->orderBy('parking_sublease.creationDate');

        return $query->getQuery()->getResult();
    }


    /**
     * @param int $dayId
     * @return ParkingSublease|null
     */
    public function findIsSubleaseOpenedByDay(int $dayId){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('day.id = :dayId')
            ->setParameter('dayId', $dayId)
            ->andWhere('parking_sublease.isSubleaseOpened = :isSubleaseOpened')
            ->setParameter('isSubleaseOpened', true)
            ->orderBy('parking_sublease.creationDate')
            ->setMaxResults(1); // here we are only interested in the first result of a tab containing all of the opened subleases of a determied day

        try {
            return $query->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }

        return null;
    }


    /**
     * @param string $yearString
     * @param string $monthString
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByDate(string $yearString, string $monthString){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('YEAR(day.date) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(day.date) = :month')
            ->setParameter('month', $monthString)
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->orderBy('parking_sublease.creationDate');

        return $query->getQuery()->getResult();
    }


    /**
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTaken(){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->innerJoin('parking_sublease.day', 'day')
            ->orderBy('day.date');

        return $query->getQuery()->getResult();
    }


    /**
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenOrOpened()
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->orWhere('parking_sublease.isSubleaseOpened = :isOpened')
            ->setParameter('isOpened', true)
            ->leftJoin('parking_sublease.day', 'day')
            ->addSelect('day')
            ->leftJoin('parking_sublease.user', 'user')
            ->addSelect('user')
            ->leftJoin('parking_sublease.taker', 'taker')
            ->addSelect('taker')
            ->orderBy('day.date', 'DESC');
        return $query->getQuery()->getResult();
    }


    /**
     * @param User $user
     * @return ParkingSublease[]
     */
    public function findTabSubleasesByUser(User $user){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.user = :user')
            ->orWhere('parking_sublease.taker = :user')
            ->setParameter('user', $user);

        return $query->getQuery()->getResult();
    }



    /**
     * @param int $loggedTakerUserId
     * @param DateTimeImmutable $beginDate
     * @param string $yearString
     * @param string $monthString
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByTakerAndByYearAndByMonthAndOrderedByDayDate(int $loggedTakerUserId, DateTimeImmutable $beginDate, string $yearString, string $monthString){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('taker.id = :loggedTakerUser')
            ->setParameter('loggedTakerUser', $loggedTakerUserId)
            ->andWhere('day.date >= :date')
            ->setParameter('date', $beginDate)
            ->andWhere('YEAR(day.date) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(day.date) = :month')
            ->setParameter('month', $monthString)
            ->orderBy('day.date');

        return $query->getQuery()->getResult();
    }


    /**
     * @param int $loggedRenterUserId
     * @param DateTimeImmutable $beginDate
     * @param string $yearString
     * @param string $monthString
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByRenterAndByYearAndByMonthAndOrderedByDayDate(int $loggedRenterUserId, DateTimeImmutable $beginDate, string $yearString, string $monthString){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.day', 'day')
            ->innerJoin('parking_sublease.user', 'renter')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('renter.id = :loggedRenterUser')
            ->setParameter('loggedRenterUser', $loggedRenterUserId)
            ->andWhere('day.date >= :date')
            ->setParameter('date', $beginDate)
            ->andWhere('YEAR(day.date) = :year')
            ->setParameter('year', $yearString)
            ->andWhere('MONTH(day.date) = :month')
            ->setParameter('month', $monthString)
            ->orderBy('day.date');

        return $query->getQuery()->getResult();
    }


    /**
     * @param int $loggedRenterUserId
     * @param DateTimeImmutable $date
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByRenter(int $loggedRenterUserId, DateTimeImmutable $date){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.user', 'renter')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('renter.id = :loggedRenterUserId')
            ->setParameter('loggedRenterUserId', $loggedRenterUserId)
            ->andWhere('day.date >= :date')
            ->setParameter('date', $date)
            ->orderBy('day.date', 'DESC');

        return $query->getQuery()->getResult();
    }


    /**
     * @param int $loggedTakerUserId
     * @param DateTimeImmutable $date
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByTaker(int $loggedTakerUserId, DateTimeImmutable $date){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('taker.id = :loggedTakerUser')
            ->setParameter('loggedTakerUser', $loggedTakerUserId)
            ->andWhere('day.date >= :date')
            ->setParameter('date', $date)
            ->orderBy('day.date', 'DESC');

        return $query->getQuery()->getResult();
    }


    /**
     * @param int $loggedRenterUserId
     * @param int $takerUserId
     * @param DateTimeImmutable $beginDate
     * @param DateTimeImmutable $endDate
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByRenterAndByTakerBetweenTwoDates(int $loggedRenterUserId, int $takerUserId,
                                                                            DateTimeImmutable $beginDate,
                                                                            DateTimeImmutable $endDate)
    {
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.user', 'renter')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('renter.id = :loggedRenterUserId')
            ->setParameter('loggedRenterUserId', $loggedRenterUserId)
            ->andWhere('taker.id = :takerUserId')
            ->setParameter('takerUserId', $takerUserId)
            ->andWhere('day.date >= :date')
            ->setParameter('date', $beginDate)
            ->andWhere('day.date <= :enddate')
            ->setParameter('enddate', $endDate)
            ->orderBy('day.date');

        return $query->getQuery()->getResult();
    }


    /**
     * @param int $loggedRenterUserId
     * @param DateTimeImmutable $beginDate
     * @param DateTimeImmutable $endDate
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByRenterBetweenTwoDates(int $loggedRenterUserId, DateTimeImmutable $beginDate, DateTimeImmutable $endDate){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.user', 'renter')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('renter.id = :loggedRenterUserId')
            ->setParameter('loggedRenterUserId', $loggedRenterUserId)
            ->andWhere('day.date >= :beginDate')
            ->setParameter('beginDate', $beginDate)
            ->andWhere('day.date <= :endDate')
            ->setParameter('endDate', $endDate)
            ->orderBy('day.date');

        return $query->getQuery()->getResult();
    }


    /**
     * @param int $loggedTakerId
     * @param DateTimeImmutable $beginDate
     * @param DateTimeImmutable $endDate
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByTakerBetweenTwoDates(int $loggedTakerId, DateTimeImmutable $beginDate, DateTimeImmutable $endDate){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.day', 'day')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('taker.id = :loggedTakerId')
            ->setParameter('loggedTakerId', $loggedTakerId)
            ->andWhere('day.date >= :beginDate')
            ->setParameter('beginDate', $beginDate)
            ->andWhere('day.date <= :endDate')
            ->setParameter('endDate', $endDate)
            ->orderBy('day.date');

        return $query->getQuery()->getResult();
    }

    /**
     * @param int $loggedTakerId
     * @param DateTimeImmutable $beginDate
     * @param DateTimeImmutable $endDate
     * @return ParkingSublease[]
     */
    public function findTabIsSubleaseTakenByTakerBetweenTwoDatesAndOrderedByRenter(int $loggedTakerId, DateTimeImmutable $beginDate, DateTimeImmutable $endDate){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->innerJoin('parking_sublease.taker', 'taker')
            ->innerJoin('parking_sublease.day', 'day')
            ->innerJoin('parking_sublease.user', 'renter')
            ->andWhere('parking_sublease.isTaken = :isTaken')
            ->setParameter('isTaken', true)
            ->andWhere('taker.id = :loggedTakerId')
            ->setParameter('loggedTakerId', $loggedTakerId)
            ->andWhere('day.date >= :beginDate')
            ->setParameter('beginDate', $beginDate)
            ->andWhere('day.date <= :endDate')
            ->setParameter('endDate', $endDate)
            ->orderBy('renter.firstName');

        return $query->getQuery()->getResult();
    }


    /**
     * @param User $user
     * @return ParkingSublease[]
     */
    public function findTabMoneyToBeReceivedSubleasesPerUser(User $user){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.user = :user')
            ->setParameter('user', $user)
            ->andWhere('parking_sublease.isTaken = :true')
            ->setParameter('true', true)
            ->andWhere('parking_sublease.paidAt IS NULL');
        return $query->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return ParkingSublease[]
     */
    public function findTabMoneyReceivedSubleasesPerUser(User $user){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.user = :user')
            ->setParameter('user', $user)
            ->andWhere('parking_sublease.isTaken = :true')
            ->setParameter('true', true)
            ->andWhere('parking_sublease.paidAt IS NOT NULL');
        return $query->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return ParkingSublease[]
     */
    public function findTabMoneyToBeReceivedAndReceivedSubleasesPerUser(User $user){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.user = :user')
            ->setParameter('user', $user)
            ->andWhere('parking_sublease.isTaken = :true')
            ->setParameter('true', true);
        return $query->getQuery()->getResult();
    }


    /**
     * @param User $taker
     * @param User $renter
     * @return ParkingSublease[]
     */
    public function findTabMoneyDueSubleasesPerTaker(User $taker, User $renter){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.taker = :taker')
            ->setParameter('taker', $taker)
            ->andWhere('parking_sublease.isTaken = :true')
            ->setParameter('true', true)
            ->andWhere('parking_sublease.user != :renter')
            ->setParameter('renter', $renter)
            ->andWhere('parking_sublease.paidAt IS NULL')
            ->innerJoin('parking_sublease.day', 'day')
            ->orderBy('day.date');


        return $query->getQuery()->getResult();
    }


    /**
     * @param User $taker
     * @return ParkingSublease[]
     */
    public function findTabMoneyPaidSubleasesPerTaker(User $taker){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.taker = :taker')
            ->setParameter('taker', $taker)
            ->andWhere('parking_sublease.isTaken = :true')
            ->setParameter('true', true)
            ->andWhere('parking_sublease.paidAt IS NOT NULL');
        return $query->getQuery()->getResult();
    }


    /**
     * @param User $user
     * @return ParkingSublease[]
     */
    public function findTabMoneyDueOrPaidSubleasesPerUser(User $user){
        $query = $this->createQueryBuilder('parking_sublease')
            ->select('parking_sublease')
            ->andWhere('parking_sublease.taker = :taker')
            ->setParameter('taker', $user)
            ->andWhere('parking_sublease.isTaken = :true')
            ->setParameter('true', true);
        return $query->getQuery()->getResult();
    }

}
