<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191031201417 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(15) NOT NULL, password VARCHAR(255) NOT NULL, last_name VARCHAR(15) NOT NULL, phone_number VARCHAR(20) DEFAULT NULL, first_name VARCHAR(15) NOT NULL, role VARCHAR(30) DEFAULT NULL, parking_number INT DEFAULT NULL, is_user_enabled TINYINT(1) NOT NULL, balance INT DEFAULT NULL, UNIQUE INDEX idx_parking_number (parking_number), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE options (id INT AUTO_INCREMENT NOT NULL, price DOUBLE PRECISION NOT NULL, partial_price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE days (id INT AUTO_INCREMENT NOT NULL, date DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', is_free_parking_sublease_by_day_quantity INT NOT NULL, UNIQUE INDEX idx_date (date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parkingsublease (id INT AUTO_INCREMENT NOT NULL, day_id INT DEFAULT NULL, user_id INT DEFAULT NULL, taker_id INT DEFAULT NULL, creation_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', sublease_parking_number INT NOT NULL, price DOUBLE PRECISION NOT NULL, is_sublease_opened TINYINT(1) NOT NULL, is_taken TINYINT(1) NOT NULL, INDEX IDX_66D682E89C24126 (day_id), INDEX IDX_66D682E8A76ED395 (user_id), INDEX IDX_66D682E8B2E74C3 (taker_id), UNIQUE INDEX idx_day_user (day_id, user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE parkingsublease ADD CONSTRAINT FK_66D682E89C24126 FOREIGN KEY (day_id) REFERENCES days (id)');
        $this->addSql('ALTER TABLE parkingsublease ADD CONSTRAINT FK_66D682E8A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE parkingsublease ADD CONSTRAINT FK_66D682E8B2E74C3 FOREIGN KEY (taker_id) REFERENCES users (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parkingsublease DROP FOREIGN KEY FK_66D682E8A76ED395');
        $this->addSql('ALTER TABLE parkingsublease DROP FOREIGN KEY FK_66D682E8B2E74C3');
        $this->addSql('ALTER TABLE parkingsublease DROP FOREIGN KEY FK_66D682E89C24126');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE options');
        $this->addSql('DROP TABLE days');
        $this->addSql('DROP TABLE parkingsublease');
    }
}
