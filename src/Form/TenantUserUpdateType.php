<?php

namespace App\Form;

use App\DTO\DTOUserRenter;
use App\DTO\DTOUserTenantUpdate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TenantUserUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'Nom d\'utilisateur :',
                'attr' => ['maxlength' => 15, 'class' => 'form-control']
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Nom :',
                'attr' => ['maxlength' => 15, 'class' => 'form-control']
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Prénom :',
                'attr' => ['maxlength' => 15, 'class' => 'form-control']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // Formulaire va avoir DTOUserRenter pour réceptacle
        $resolver->setDefault("data_class", DTOUserTenantUpdate::class);
    }
}
