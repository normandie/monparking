<?php


namespace App\ControllerHelpers;


use App\Entity\Day;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Repository\ParkingSubleaseRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Exception;
use Symfony\Component\Validator\Constraints\Date;

class CalendarData
{

    /**
     * @var array
     * table with the name of each month written in french,
     * usefull to show the month name on the calendar web page.
     */
    public $months = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Décembre'
    ];


    /**
     * @var array
     */
    public $monthsLowCase = [
        'janvier',
        'février',
        'mars',
        'avril',
        'mai',
        'juin',
        'juillet',
        'août',
        'septembre',
        'octobre',
        'novembre',
        'décembre'
    ];


    /**
     * @var int|null
     */
    private $month;


    /**
     * @return int
     */
    public function getMonth(): int
    {
        return $this->month;
    }

    /**
     * @var int|null
     */
    private $year;


    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }


    /**
     * @var array
     */
    private $dayDataList;


    /**
     * @var array
     */
    private $datesList;



    /**
     * @return DayData[]
     */
    public function getDayDataList(): array
    {
        return $this->dayDataList;
    }


    /**
     * @return DateTimeImmutable[]
     */
    public function getDatesList(): array
    {
        return $this->datesList;
    }


    /**
     * @var
     */
    private $isEmployee;

    public function getIsEmployee(): bool
    {
        return $this->isEmployee;
    }

    /**
     * Month constructor.
     * Month is between 1 and 12
     * @param Day[] $allDays
     * @param int|null $year
     * @param int|null $month
     * @throws Exception
     */
    public function __construct(array $allDays, ?int $year = null, ?int $month = null)
    {
        if ($year === null) {
            $year = intval(date('Y'));
        }

//        ternaire :
//        $this->year = $year ?: intval(date('Y')))

        if ($month === null || $month < 1 || $month > 12) {
            $month = intval(date('m'));
        }

        $this->year = $year;
        $this->month = $month;
        $this->dayDataList = $this->createDayDataList($year, $month, $allDays);
        $this->datesList = $this->createDatesList($year, $month);
    }


    /**
     * returns the first day of the month
     * @return DateTimeImmutable
     * @throws Exception
     */
    public function getStartingDay(): DateTimeImmutable
    {
        return (new DateTimeImmutable())->setDate($this->year, $this->month, 1);
    }


    /**
     * $this->months retourne le numéro du mois en cours
     * permet de naviguer d'un mois à l'autre
     * returns the month name
     * @return string
     */
    public function toString(): string
    {
        return $this->months[$this->month - 1] . ' ' . $this->year;
    }


    public function toStringLowCase(): string
    {
        return $this->monthsLowCase[$this->month - 1] . ' ' . $this->year;
    }

    public function toStringLowCasePreviousMonth(): string
    {
        $currentDate = new DateTimeImmutable('midnight');
        $currentYearLessTwo = $currentDate->modify('-2 month');
        $previousMonthDateString = $currentYearLessTwo->format('m');
        $YearPreviousMonthDateString = $currentYearLessTwo->format('Y');

        return $this->monthsLowCase[$previousMonthDateString] . ' ' . $YearPreviousMonthDateString;
    }


    public function toStringFromMonthAndYearString(string $yearMonth): string
    {
        $month = substr($yearMonth, -2);
        $year = substr($yearMonth, 0, 4);
        $intMonth = (int)$month;
        $intYear = (int)$year;
        return $this->months[$intMonth-1] . ' ' . $intYear;
    }

    public function toStringLowCaseFromMonthAndYearString(string $yearMonth): string
    {
        $month = substr($yearMonth, -2);
        $year = substr($yearMonth, 0, 4);
        $intMonth = (int)$month;
        $intYear = (int)$year;
        return $this->monthsLowCase[$intMonth-1] . ' ' . $intYear;
    }


    /**
     * Renvoie le nombre de semaines dans le mois
     * @return int
     * @throws Exception
     */
    public function getWeeks(): int
    {
        $start = $this->getStartingDay();
        $end = $start->modify('+1 month -1 day');
        $startWeek = intval($start->format('W'));
        $endWeek = intval($end->format('W'));
        if ($endWeek === 1) {
            $endWeek = intval($end->modify('- 7 days')->format('W')) + 1;
        }
        $weeks = $endWeek - $startWeek + 1;
        if ($weeks < 0) {
            $weeks = intval($end->format('W'));
        }
//        $weeks = 6;
        return $weeks;
    }

    /**
     * Est-ce que le jour est dans le mois en cours
     * @param DateTimeImmutable $date
     * @return bool
     * @throws Exception
     */
    public function withinMonth(\DateTimeImmutable $date): bool
    {
        return $this->getStartingDay()->format('Y-m') === $date->format('Y-m');
    }

    /**
     * Renvoie le mois suivant
     * @return CalendarData
     * @throws Exception
     */
    public function nextMonth(): CalendarData
    {
        $month = $this->month + 1;
        $year = $this->year;
        if ($month > 12) {
            $month = 1;
            $year += 1;
        }
        return new CalendarData([], $year, $month);
    }


    /**
     * Renvoie le mois précédent
     * @return CalendarData
     * @throws Exception
     */
    public function previousMonth(): CalendarData
    {
        $month = $this->month - 1;
        $year = $this->year;
        if ($month < 1) {
            $month = 12;
            $year -= 1;
        }
        return new CalendarData([], $year, $month);
    }


    /**
     * $allDays is the list of the the existing Days taken from the repository
     * $date is the listed date from 1 to 30/31 of each month
     */
    private function getDayFromDate(array $allDays, DateTimeImmutable $date) : ?Day
    {
        foreach($allDays as $day){
            if ($day->getDate()->format('Y-m-d') === $date->format('Y-m-d')){
                return $day;
            }
        }
        return null;
    }

    /**
     * @param int $year
     * @param int $month
     * @param array $allDays
     * @return array
     * @throws Exception
     */
    private function createDayDataList(int $year, int $month, array $allDays): array
    {
        $dayDataList = [];
        $startOfMonth = (new DateTimeImmutable())->setDate($year, $month, 1)->setTime(0, 0, 0, 0);
        $endOfMonth = $startOfMonth->modify('+1 month')->modify('-1 days');
        while ($startOfMonth <= $endOfMonth) {
//            $formattedDate = $startOfMonth->format('Y-m-d');

            $existingDay = $this->getDayFromDate($allDays, $startOfMonth);

            if ($existingDay !== null) {

                /**
                 * $dayDataList contains variables of type DayData wich contain
                 * each one two information; first : a DateTimeImmutable type date variable,
                 * second : a Day type variable
                 */
                $dayDataList[] = new DayData($startOfMonth, $existingDay);
            }


            /**
             * $startOfMonth is incremented by one day at each loop
             * until it reaches the last day of the month
             */
            $startOfMonth = $startOfMonth->modify('+1 days');
        }
        return $dayDataList;
    }


    private function createDatesList(int $year, int $month): array
    {
        $datesList = [];
        $startOfMonth = (new DateTimeImmutable())->setDate($year, $month, 1)->setTime(0, 0, 0, 0);
        $endOfMonth = $startOfMonth->modify('+1 month')->modify('-1 days');
        while ($startOfMonth <= $endOfMonth) {
//            $formattedDate = $startOfMonth->format('Y-m-d');

            $datesList[] = $startOfMonth;

            /**
             * $startOfMonth is incremented by one day at each loop
             * until it reaches the last day of the month
             */
            $startOfMonth = $startOfMonth->modify('+1 days');
        }

        return $datesList;
    }


    /**
     * @param array $dayDataList
     * @return array
     */
    public function getOpenToSubleaseDaysQuantityPerDayDateList(array $dayDataList): array
    {
        /* @var DayData $day */
        $list[] = null;
        foreach ($dayDataList as $day) {
            if ($day->getDay() !== null && $day->getIsFreeParkingSubleaseByDay() > 0) {
                $date = $day->getDate();
                $dateString = $date->format('Y-m-d');
                $numberOfFreedDays = (string)$day->getIsFreeParkingSubleaseByDay();
                $list[$dateString] = $numberOfFreedDays;
            }
        }

        return $list;
    }


    /**
     * @param array $dayDataList
     * @param DateTimeImmutable $today
     * @return int
     */
    public function getTotalOpenToSubleaseDaysQuantityPerDayDateList(array $dayDataList, DateTimeImmutable $today): int
    {
        $counter = 0;


        /**
         * @var DayData $last
         */
        $last = end($dayDataList);

        if ($last !== false && $last->getDate() >= $today) {

            /**
             * @var DayData $day
             */
            foreach ($dayDataList as $day) {

                if ($day->getDay() !== null && $day->getIsFreeParkingSubleaseByDay() > 0 && $day->getDate() >= $today) {
                    $counter += $day->getIsFreeParkingSubleaseByDay();
                }
            }
        }

        return $counter;
    }



    /**
     * @param array $dayDataList
     * @param DateTimeImmutable $today
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param User $loggedInTakerUser
     * @return int
     */
    public function getTotalByTakerOpenToSubleaseDaysQuantityPerDayDateList(array $dayDataList, DateTimeImmutable $today, ParkingSubleaseRepository $parkingSubleaseRepository, User $loggedInTakerUser): int
    {
        $counter = 0;

        /**
         * @var DayData $last
         */
        $last = end($dayDataList);

        if ($last !== false && $last->getDate() >= $today) {

            /**
             * @var DayData $day
             */
            foreach ($dayDataList as $day) {

                if ($day->getDay() !== null && $day->getIsFreeParkingSubleaseByDay() > 0 && $day->getDate() >= $today) {
                    if ($parkingSubleaseRepository->findIsSubleaseTakenByDayAndByTaker($day->getDayId(), $loggedInTakerUser->getId()) === null) {
                        $counter += 1;
                    }
                }
            }
        }
//        dd($counter);
        return $counter;
    }




    /**
     * @param array $dayDataList
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param User $user
     * @return array
     */
    public function getExistingOrHasExistedOpenedSubleaseByDayAndByUserList(array $dayDataList, ParkingSubleaseRepository $parkingSubleaseRepository, User $user){

        /* @var DayData $day */
        $list[] = null;
        $existsOrHasExistedSublease = null;

        foreach ($dayDataList as $day) {
//            dd($day->getDayId());
            $existsOrHasExistedSublease = $parkingSubleaseRepository->findOpenedSubleaseByDayAndByUser($day->getDayId(), $user->getId());

            if ($existsOrHasExistedSublease !== null) {
                $date = $day->getDate();
                $dateString = $date->format('Y-m-d');
                $list[$dateString] = $existsOrHasExistedSublease;
            }
        }

        return $list;
    }


    /**
     * @param array $dayDataList
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param User $user
     * @return array
     */
    public function getExistingOpenedSubleaseByDayAndByUserList(array $dayDataList, ParkingSubleaseRepository $parkingSubleaseRepository, User $user){

        /* @var DayData $day */
        $list[] = null;
        $existsOpenedSublease = null;
        foreach ($dayDataList as $day){
            $existsOpenedSublease = $parkingSubleaseRepository->findIsSubleaseOpenedByDayAndByUser($day->getDayId(), $user->getId());
            if ($existsOpenedSublease !== null){
                $date = $day->getDate();
                $dateString = $date->format('Y-m-d');
                $list[$dateString] = $existsOpenedSublease;
            }
        }
        return $list;
    }


    /**
     * @param array $dayDataList
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param User $taker
     * @return array
     */
    public function getExistingTakenSubleaseByDayAndByTakerList(array $dayDataList, ParkingSubleaseRepository $parkingSubleaseRepository, User $taker){

        /* @var DayData $day */
        $list[] = null;
        $existsTakenSublease = null;
        foreach ($dayDataList as $day) {
            $existsTakenSublease = $parkingSubleaseRepository->findIsSubleaseTakenByDayAndByTaker($day->getDayId(), $taker->getId());
            if ($existsTakenSublease !== null) {
                $date = $day->getDate();
                $dateString = $date->format('Y-m-d');
                $list[$dateString] = $existsTakenSublease;
            }
        }
        return $list;
    }


    /**
     * @param array $dayDataList
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @return array
     */
    public function getIsSubleaseOpenedByDay(array $dayDataList, ParkingSubleaseRepository $parkingSubleaseRepository){

        /* @var DayData $day */
        $list[] = null;
        $existsSublease = null;

        foreach ($dayDataList as $day) {
            $existsSublease = $parkingSubleaseRepository->findIsSubleaseOpenedByDay($day->getDayId());
            if ($existsSublease !== null) {
                $date = $day->getDate();
                $dateString = $date->format('Y-m-d');
                $list[$dateString] = $existsSublease;
            }
        }
        return $list;
    }


    /**
     * @param array $dayDataList
     * @return array
     */
    public function getTakenOrOpenedSubleaseByDateList(array $dayDataList){

        /* @var DayData $day */
        $list[][] = null;

        /* @var ParkingSublease $subleasesListFromDayData */
        $subleasesListFromDayData = null;

        foreach ($dayDataList as $day) {
            $subleasesListFromDayData = $day->getDay()->getParkingSublease();
            if ($subleasesListFromDayData !== null) {

                /**
                 * @var ParkingSublease $sublease
                 */
                foreach ($subleasesListFromDayData as $sublease){
                    $date = $day->getDate();
                    $dateString = $date->format('Y-m-d');
                    if ($sublease->getIsTaken() || $sublease->getIsSubleaseOpened()){
                        $list[$dateString][] = $sublease;
                    }
                }
            }
        }
        return $list;
    }
}
