<?php


namespace App\ControllerHelpers\Security;


use App\Entity\User;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class SecurityUser implements UserInterface, EquatableInterface
{


    /**
     * @var User
     */
    private $user;


    /**
     * @var int
     */
    private $userId;

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }


    /**
     * SecurityUser constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->userId = $user->getId();
    }

    public function getIdentifier(): int
    {
        return $this->userId;
    }


    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }


    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * @param UserInterface $user
     * @return bool
     */
    public function isEqualTo(UserInterface $user): bool
    {
        if (!$user instanceof self) {
            return false;
        }

        if ($this->getIdentifier() !== $user->getIdentifier()) {
            return false;
        }

        return true;
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return array (Role|string)[] The user roles
     */
    public function getRoles(): array
    {
        return [$this->user->getRole()];
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword(): string
    {
        return $this->user->getPassword();
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername() : string
    {
        return $this->user->getUsername();
    }


    /**
     * @return string
     */
    public function getLastName() : string
    {
        return $this->user->getLastName();
    }


    /**
     * @return string
     */
    public function getFirstName() : string
    {
        return $this->user->getFirstName();
    }


    /**
     * @return string
     */
    public function getRole(): string
    {
        if ($this->user->getRole() === 'ROLE_TENANT') {
            return 'ROLE_TENANT';
        }elseif ($this->user->getRole() === 'ROLE_RENTER'){
            return 'ROLE_RENTER';
        }elseif ($this->user->getRole() === 'ROLE_ADMINISTRATOR'){
            return 'ROLE_ADMINISTRATOR';
        }

        return 'Visitor';
    }


    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // Unused, there is nothing to hide.
    }

    public function serialize(): ?string
    {
        return json_encode($this->userId);
    }


    public function unserialize($serialized): void
    {
        $this->userId = json_decode($serialized);
    }

}