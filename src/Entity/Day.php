<?php

namespace App\Entity;

use App\DTO\DTODays;
use App\Repository\ParkingSubleaseRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use LogicException;

//deux Day ne doivent pas pouvoir avoir la même date.
/**
 * @ORM\Entity(repositoryClass="App\Repository\DayRepository")
 * @Table(name="days",uniqueConstraints={@UniqueConstraint(name="idx_date", columns={"date"})})
 */
class Day
{

    //=========================================================================
    // Constants
    //=========================================================================

    public const VACATION = 1;
    public const REST = 2;
    public const HOURLY_BALANCE = 3;
    public const UNPAID = 4;


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @ORM\Column(type="date_immutable")
     * @OrderBy({"name" = "ASC"})
     */
    private $date;


    /**
     * @return DateTimeImmutable
     */
    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }



    /**
     * @var Collection|null
     * @ORM\OneToMany(targetEntity="ParkingSublease", mappedBy="day", cascade={"persist"})
     */
    private $parkingSublease;



    /**
     * @return Collection
     */
    public function getParkingSublease(): Collection
    {
        return $this->parkingSublease;
    }


    public function getIsSubleasedSubleaseByDayAndUser(ParkingSubleaseRepository $parkingSubleaseRepository, ?Day $day, User $user)
    {
        if ($day !== null) {
            return $parkingSubleaseRepository->findIsSubleaseOpenedByDayAndByUser($day->getId(), $user->getId());
        }
        return null;
    }



    /**
     * @ORM\Column(type="integer")
     */
    private $isFreeParkingSubleaseByDayQuantity;


    /**
     * @return int|null
     */
    public function getIsFreeParkingSubleaseByDayQuantity(): ?int
    {
        return $this->isFreeParkingSubleaseByDayQuantity;
    }



    public function isFreeParkingSubleaseByDayIncreaseByOne(): void
    {
        $this->isFreeParkingSubleaseByDayQuantity++;
    }

    public function isFreeParkingSubleaseByDayDecreaseByOne(): void
    {
        if ($this->isFreeParkingSubleaseByDayQuantity > 0) {
            $this->isFreeParkingSubleaseByDayQuantity--;
        }
    }



//    pas de * @ORM\JoinColumn(nullable=false) car on ne crée pas de colonne dans la table
//'attribut fetch="EAGER" pour forcer le chargement en même temps que la récupération de l'entité



    //=========================================================================
    // Constructor
    //=========================================================================

    public function __construct(DateTimeImmutable $date)
    {
        $this->date = $date;
        $this->parkingSublease = new ArrayCollection();
        $this->isFreeParkingSubleaseByDayQuantity = 0;
    }

    public function __toString()
    {
        return $this->date->format('d-m-Y');
    }


}
