<?php


namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use LogicException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @Table(name="users",uniqueConstraints={@UniqueConstraint(name="idx_parking_number", columns={"parking_number"})})
 */
class User
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $username;

    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param $newUsername
     */
    public function changeUsername($newUsername)
    {
        $this->username = $newUsername;
    }


    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $revolutPhoneNumber;


    /**
     * @return string|null
     */
    public function getRevolutPhoneNumber(): ?string
    {
        return $this->revolutPhoneNumber;
    }


    /**
     * @param string|null $newRevolutPhoneNumber
     */
    public function changeRevolutPhoneNumber(?string $newRevolutPhoneNumber): void
    {
        $this->revolutPhoneNumber = $newRevolutPhoneNumber;
    }


    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $payPalEmail;


    /**
     * @return string|null
     */
    public function getPayPalEmail(): ?string
    {
        return $this->payPalEmail;
    }


    /**
     * @param string $newPayPalEmail
     */
    public function changePayPalEmail(?string $newPayPalEmail): void
    {
        $this->payPalEmail = $newPayPalEmail;
    }


    /**
     * @param $newPhoneNumber
     */
    public function changePhoneNumber($newPhoneNumber)
    {
        $this->phoneNumber = $newPhoneNumber;
    }


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min = 6,
     *     minMessage = "Le mot de passe doit contenir au moins 6 caractères",
     *     maxMessage = "Le mot de passe ne doit pas dépasser 50 caractères",
     *     )
     */
    private $password;

    public function getPassword(): string
    {
        return $this->password;
    }


    /**
     * @param string $newPassword
     */
    public function changePassword(string $newPassword): void
    {
        $this->password = password_hash($newPassword, PASSWORD_BCRYPT, ['cost' => 12]);
    }


    /**
     * @var Collection|null
     * @ORM\OneToMany(targetEntity="ParkingSublease", mappedBy="user", cascade={"persist"})
     */
    private $parkingSublease;

    /**
     * @var Collection|null
     * @ORM\OneToMany(targetEntity="ParkingSublease", mappedBy="taker", cascade={"persist"})
     */
    private $parkingSubleaseAsTaker;


    /**
     * @ORM\Column(type="string", length=15)
     */
    private $lastName;


    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $phoneNumber;


    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }


    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }


    /**
     * @param string $newLastName
     */
    public function changeLastName(string $newLastName)
    {
        $this->lastName = $newLastName;
    }


    /**
     * @ORM\Column(type="string", length=15)
     */
    private $firstName;

    public function getFirstName(): string
    {
        return $this->firstName;
    }


    /**
     * @param $newFirstName
     */
    public function changeFirstName($newFirstName)
    {
        $this->firstName = $newFirstName;
    }

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $role;


    public function getRole(): ?string
    {
        return $this->role;
    }


    /**
     * @ORM\Column(type="integer", length=4, nullable=true)
     */
    private $parkingNumber;


    /**
     * @return int|null
     */
    public function getParkingNumber(): ?int
    {
        return $this->parkingNumber;
    }


    /**
     * @param int|null $number
     */
    public function setParkingNumber(int $number): void
    {
        $this->parkingNumber = $number;
    }


    /**
     * @param int $newParkingNumber
     */
    public function changeParkingNumber(int $newParkingNumber): void
    {
        $this->parkingNumber = $newParkingNumber;
    }


    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $isUserEnabled;


    public function setIsUserEnabled(): void
    {
        $this->isUserEnabled = true;
    }


    public function setIsUserNotEnabled(): void
    {
        $this->isUserEnabled = false;
    }


    /**
     * @return bool
     */
    public function getIsUserEnabled(): bool
    {
        return $this->isUserEnabled;
    }


    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    private $balance;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Messages", mappedBy="transmitter")
     */
    private $sentMessages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Messages", mappedBy="receiver")
     */
    private $receivedMessages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UsersLogsEvents", mappedBy="loggedInUser")
     */
    private $LoginEvents;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isUserBlockedForInsolvency;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $insolvencyImmunity;



    /**
     * @return int
     */
    public function getBalance(): int
    {
        return $this->balance;
    }


    public function buyParking(ParkingSublease $parkingSublease): void
    {
        if($this->role === 'ROLE_TENANT') {
            $this->balance -= $parkingSublease->getPrice();
        }else{
            throw new LogicException('Only a ROLE_TENANT user can buy a parking');
        }
    }


    public function sellParking(ParkingSublease $parkingSublease): void
    {
        if($this->role === 'ROLE_RENTER') {
            $this->balance += $parkingSublease->getPrice();
        }else{
            throw new LogicException('Only a ROLE_RENTER user can sell a parking');
        }
    }


    /**
     * @param int $amount
     */
    public function setBalance(int $amount): void
    {
        $this->balance = $amount;
    }


//    public function promoteToRenter(){
//        if ($this->role==='ROLE_RENTER'){
//            throw new LogicException('The user role is already ROLE_RENTER');
//        }
//
//        $this->role = 'ROLE_RENTER';
//    }
//
//
//    public function promoteToTenant(){
//        if ($this->role==='ROLE_TENANT'){
//            throw new LogicException('The user role is already ROLE_TENANT');
//        }
//
//        $this->role = 'ROLE_TENANT';
//    }
//
//
//    public function promoteToAdministrator(){
//        if ($this->role==='ROLE_ADMINISTRATOR'){
//            throw new LogicException('The user role is already ROLE_ADMINISTRATOR');
//        }
//
//        $this->role = 'ROLE_ADMINISTRATOR';
//    }


    public function __construct(string $username, string $password, string $lastName, string $firstName, string $role)
    {
        $this->username = $username;
        $this->changePassword($password);
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->role = $role;
        $this->balance = 0;
        $this->setIsUserEnabled();
        $this->sentMessages = new ArrayCollection();
        $this->receivedMessages = new ArrayCollection();
        $this->LoginEvents = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->firstName.' '.$this->lastName;
    }

    /**
     * @return Collection|Messages[]
     */
    public function getSentMessages(): Collection
    {
        return $this->sentMessages;
    }

    public function addSentMessage(Messages $sentMessage): self
    {
        if (!$this->sentMessages->contains($sentMessage)) {
            $this->sentMessages[] = $sentMessage;
            $sentMessage->setTransmitter($this);
        }

        return $this;
    }

    public function removeSentMessage(Messages $sentMessage): self
    {
        if ($this->sentMessages->contains($sentMessage)) {
            $this->sentMessages->removeElement($sentMessage);
            // set the owning side to null (unless already changed)
            if ($sentMessage->getTransmitter() === $this) {
                $sentMessage->setTransmitter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Messages[]
     */
    public function getReceivedMessages(): Collection
    {
        return $this->receivedMessages;
    }

    public function addReceivedMessage(Messages $receivedMessage): self
    {
        if (!$this->receivedMessages->contains($receivedMessage)) {
            $this->receivedMessages[] = $receivedMessage;
            $receivedMessage->setReceiver($this);
        }

        return $this;
    }

    public function removeReceivedMessage(Messages $receivedMessage): self
    {
        if ($this->receivedMessages->contains($receivedMessage)) {
            $this->receivedMessages->removeElement($receivedMessage);
            // set the owning side to null (unless already changed)
            if ($receivedMessage->getReceiver() === $this) {
                $receivedMessage->setReceiver(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UsersLogsEvents[]
     */
    public function getLoginEvents(): Collection
    {
        return $this->LoginEvents;
    }

    public function addLoginEvent(UsersLogsEvents $loginEvent): self
    {
        if (!$this->LoginEvents->contains($loginEvent)) {
            $this->LoginEvents[] = $loginEvent;
            $loginEvent->setLoggedInUser($this);
        }

        return $this;
    }

    public function removeLoginEvent(UsersLogsEvents $loginEvent): self
    {
        if ($this->LoginEvents->contains($loginEvent)) {
            $this->LoginEvents->removeElement($loginEvent);
            // set the owning side to null (unless already changed)
            if ($loginEvent->getLoggedInUser() === $this) {
                $loginEvent->setLoggedInUser(null);
            }
        }

        return $this;
    }

    public function getIsUserBlockedForInsolvency(): ?bool
    {
        return $this->isUserBlockedForInsolvency;
    }

    public function setIsUserBlockedForInsolvency(?bool $isUserBlockedForInsolvency): self
    {
        $this->isUserBlockedForInsolvency = $isUserBlockedForInsolvency;

        return $this;
    }

    public function getInsolvencyImmunity(): ?bool
    {
        return $this->insolvencyImmunity;
    }

    public function setInsolvencyImmunity(?bool $insolvencyImmunity): self
    {
        $this->insolvencyImmunity = $insolvencyImmunity;

        return $this;
    }



}

