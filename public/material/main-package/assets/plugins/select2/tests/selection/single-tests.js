module('Selection containers - Single');

var SingleSelection = require('select2/selection/single');

var $ = require('jquery');
var Options = require('select2/options');
var Utils = require('select2/utils');

var options = new Options({});

test('display uses templateSelection', function (assert) {
  var called = false;

  var templateOptions = new Options({
    templateSelection: function (data) {
      called = true;

      return data.text;
    }
  });

  var selection = new SingleSelection(
    $('#qunit-fixture .single'),
    templateOptions
  );

  var out = selection.display({
    text: 'test'
  });

  assert.ok(called);

  assert.equal(out, 'test');
});

test('templateSelection can addClass', function (assert) {
  var called = false;

  var templateOptions = new Options({
    templateSelection: function (data, container) {
      called = true;
      container.addClass('testclass');
      return data.text;
    }
  });

  var selection = new SingleSelection(
    $('#qunit-fixture .single'),
    templateOptions
  );

  var $container = selection.selectionContainer();
  
  var out = selection.display({
    text: 'test'
  }, $container);

  assert.ok(called);

  assert.equal(out, 'test');
  
  assert.ok($container.hasClass('testclass'));
});

test('empty Update clears the selection', function (assert) {
  var selection = new SingleSelection(
    $('#qunit-fixture .single'),
    options
  );

  var $selection = selection.render();
  var $rendered = $selection.find('.select2-selection__rendered');

  $rendered.text('testing');

  selection.Update([]);

  assert.equal($rendered.text(), '');
});

test('Update renders the data text', function (assert) {
  var selection = new SingleSelection(
    $('#qunit-fixture .single'),
    options
  );

  var $selection = selection.render();
  var $rendered = $selection.find('.select2-selection__rendered');

  selection.Update([{
    text: 'test'
  }]);

  assert.equal($rendered.text(), 'test');
});

test('escapeMarkup is being used', function (assert) {
  var selection = new SingleSelection(
    $('#qunit-fixture .single'),
    options
  );

  var $selection = selection.render();
  var $rendered = $selection.find('.select2-selection__rendered');

  var unescapedText = '<script>bad("stuff");</script>';

  selection.Update([{
    text: unescapedText
  }]);

  assert.equal(
    $rendered.text(),
    unescapedText,
    'The text should be escaped by default to prevent injection'
  );
});
